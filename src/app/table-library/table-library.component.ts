import { Books } from './../models/books';
import { LibraryCrudService } from './../services/libraryCrud.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: "app-table-library",
  templateUrl: "./table-library.component.html",
  styleUrls: ["./table-library.component.css"]
})
export class TableLibraryComponent implements OnInit {
  name: string;
  author: string;
  value: number;
  data: Books[] = [];


  constructor(private api: LibraryCrudService) {}

  ngOnInit() {
    this.api.getTodos().subscribe(
      res => {
        this.data = res;
        console.log(this.data);
      },
      err => {
        console.log(err);
      }
    );
  }
  deleteTodo(id, index) {
    this.api.deleteTodo(id).subscribe(
      res => {
        this.data.splice(index, 1);
      },
      err => {
        console.log(err);
      }
    );
  }
}



