import { LibraryCrudService } from './../../services/libraryCrud.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators, NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';


@Component({
  selector: "app-edit-component",
  templateUrl: "./edit-component.component.html",
  styleUrls: ["./edit-component.component.css"]
})
export class EditComponentComponent implements OnInit {
  todoForm: FormGroup;
  id: number;
  name: string;
  author: string;
  value: number;
  arrItem = [];
  constructor(
    private formBuilder: FormBuilder,
    private activeAouter: ActivatedRoute,
    private router: Router,
    private api: LibraryCrudService
  ) {}

  ngOnInit() {
    this.getDetail(this.activeAouter.snapshot.params.id);
    this.todoForm = this.formBuilder.group({
      index: [{ value: null, disabled: true }],
      name: ["", Validators.compose([Validators.required])],
      author: ["", Validators.compose([Validators.required])],
      value: ["", Validators.compose([Validators.required])]
    });
    console.log(this.todoForm.value);

  }
  getDetail(id) {
    this.api.getTodo(id).subscribe(data => {
      this.id = data.id;
      this.name = data.name;
      this.author = data.author;
      this.value = data.value;
      });

  }


updateTodo(form: NgForm) {

    this.api.updateTodo(this.id, form).subscribe(
      res => {
        this.router.navigate(["/livros"]);

      },

      err => {
        console.log(err);
      }
    );
  }
}
