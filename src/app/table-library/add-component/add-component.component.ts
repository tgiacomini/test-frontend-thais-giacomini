import { LibraryCrudService } from './../../services/libraryCrud.service';
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';

@Component({
  selector: 'app-add-component',
  templateUrl: './add-component.component.html',
  styleUrls: ['./add-component.component.css']
})
export class AddComponentComponent implements OnInit {
  id: number;
  todoForm: FormGroup;
  constructor(
    private formBuilder: FormBuilder,
    private router: Router,
    private api: LibraryCrudService
  ) {}

  ngOnInit() {
    this.todoForm = this.formBuilder.group({
      name: ['', Validators.compose([Validators.required])],
      author: ['', Validators.compose([Validators.required])],
      value: ['', Validators.compose([Validators.required])]
    });
  }

  addTodo() {
    const payload = {
      name: this.todoForm.controls.name.value,
      author: this.todoForm.controls.author.value,
      value: this.todoForm.controls.value.value
    };

    console.log(payload);
    this.api.addTodo(payload).subscribe(
      res => {
        const id = res['id'];
        this.router.navigate(['./livros']);
      },
      err => {
        console.log(err);
      }
    );
  }


}
