import { EditComponentComponent } from './table-library/edit-component/edit-component.component';
import { AddComponentComponent } from './table-library/add-component/add-component.component';
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {NgbModule} from '@ng-bootstrap/ng-bootstrap';
import { AppRoutingModule } from './app-routing.module';
import { CommonModule } from '@angular/common';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http';
// tslint:disable-next-line: no-unused-expression
import { TableLibraryComponent } from './table-library/table-library.component';
import { ReactiveFormsModule, FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { NavBarComponent } from './navBar/navBar.component';

@NgModule({
   declarations: [
      AppComponent,
      TableLibraryComponent,
      AddComponentComponent,
      EditComponentComponent,
      NavBarComponent
   ],
   imports: [
      BrowserModule,
      CommonModule,
      AppRoutingModule,
      NgbModule,
      ReactiveFormsModule,
      FormsModule
   ],
   exports: [
      TableLibraryComponent,
      ReactiveFormsModule,
      FormsModule,
      HttpClientModule,
      RouterModule
   ],
   providers: [],
   bootstrap: [
      AppComponent
   ]
})
export class AppModule {}
