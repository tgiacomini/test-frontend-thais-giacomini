import { EditComponentComponent } from './table-library/edit-component/edit-component.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { TableLibraryComponent } from './table-library/table-library.component';
import { AddComponentComponent } from './table-library/add-component/add-component.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: '/livros',
    pathMatch: 'full'
  },
  {
    path: 'livros-add',
    component: AddComponentComponent,
    data: { title: 'Adicionar Livro' }
  },
  {
    path: 'livros-editar/:id',
    component: EditComponentComponent,
    data: { title: 'Editar Livro' }
  },
  {
    path: 'livros',
    component: TableLibraryComponent,
    data: { title: 'Tabela de livros' }
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
