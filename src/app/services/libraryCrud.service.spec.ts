/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { LibraryCrudService } from './libraryCrud.service';

describe('Service: LibraryCrud', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [LibraryCrudService]
    });
  });

  it('should ...', inject([LibraryCrudService], (service: LibraryCrudService) => {
    expect(service).toBeTruthy();
  }));
});
