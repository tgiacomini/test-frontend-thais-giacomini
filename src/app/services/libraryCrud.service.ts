import { Books } from './../models/books';
import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import {
  HttpClient,
  HttpErrorResponse,
  HttpHeaders
} from '@angular/common/http';
import { catchError, tap, map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class LibraryCrudService {
  constructor(private http: HttpClient) {}


  apiUrl = 'http://localhost:3000/posts';


  httpOptions = {
    headers: new HttpHeaders({ 'Content-Type': 'application/json' })
  };

  getTodos(): Observable<Books[]> {
    return this.http.get<Books[]>(this.apiUrl, this.httpOptions).pipe(
      catchError(this.handleError('Get livros', []))
    );
  }
  getTodo(id: number): Observable<Books> {
    const url = `${this.apiUrl}/${id}`;
    return this.http.get<Books>(url).pipe(
      tap(_ => console.log(`fetched todo id=${id}`)),
      catchError(this.handleError<Books>(`getTodo id=${id}`))
    );
  }

  addTodo(book: any): Observable<Books> {
    return this.http.post<Books>(`${this.apiUrl}`, book, this.httpOptions).pipe(
      tap((book: Books) => console.log(`Livro adicionado id=${book.id}`)),
      catchError(this.handleError<Books>('addTodo'))
    );
  }

  updateTodo(id, book): Observable<any> {
    const url = `${this.apiUrl}/${id}`;
    return this.http.put(url, book, this.httpOptions).pipe(
      tap(_ => console.log(`updated todo id=${id}`)),
      catchError(this.handleError<any>('Livro atualizado'))
    );
  }

  deleteTodo(id): Observable<Books> {
    const url = `${this.apiUrl}/${id}`;

    return this.http.delete<Books>(url, this.httpOptions).pipe(
      tap(_ => console.log(`deleted todo id=${id}`)),
      catchError(this.handleError<Books>('Livro deletado'))
    );
  }
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}
